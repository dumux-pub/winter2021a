close all;
clear all;
% When testing hypotheses
% Null hypothesis: The dependency between the two parameters is random, does not exist.
% Alternative hypothesis: The two parameters have a relationship that is
% not random. 

% load the csv files of all the points in the domain

filename1 = ('darcy_a.csv'); % reference values - Darcy
A = readtable(filename1);

filename2 = ('forch1_a.csv'); % new values - Forchheimer Case 1
B = readtable(filename2);

filename3 = ('forch2_a.csv'); % new values - Forchheimer Case 2
C = readtable(filename3);

% forchConst [ - ]
c_F =  0.55;

% forchCoeff constant [ m ]
c_beta =  1.52e-4;

% tortuosity [ - ]
C.tau = 1./C.porosity; 

% liquid dynamic viscosity [Pa*s] - pure water
miu_liq = 1e-3;

% gas dynamic viscosity [Pa*s] - steam
miu_gas = 1e-5;

% relative permeabilities [ - ]
C.kr_gas = (C.mob_gas.*miu_gas);
C.kr_liq = (C.mob_liq.*miu_liq);

% beta_c - [ m^-1 ] for Forchheimer Case 1
B.beta_c = (c_F./sqrt(B.permeability));
B.beta_c_model(:,1) = c_F;

% beta_a [ m^-1 ] - for Forchheimer Case 2
C.beta_gas = (c_beta .* C.tau)./((C.permeability.*C.kr_gas).*(C.porosity .* (1.-C.S_liq)));
C.beta_liq = (c_beta .* C.tau)./((C.permeability.*C.kr_liq).* (C.porosity .* C.S_liq));
C.beta_liq_model = (c_beta .* C.tau)./ (C.porosity .* C.S_liq);

C.beta_liq(isinf(C.beta_liq)) = 1e+12; % to omit Inf values caused by dividing with zero, S_liq = 0.1 and S_gas = 0.9

% Summary statistics
S1 = summary(A);
S2 = summary(B);
S3 = summary(C);

% % Filter with porosity
% rows1 = (C.porosity == 0.94 | C.porosity == 0.90);
% D = C(rows1,:);
% 
% rows2 = (C.porosity ~= 0.94);
% E = C(rows2,:);

% rows3 = (C.beta_liq > 9.328090841482436e+11);
% max_beta_liq = C(rows3,:);

% Distance correlation coefficient 
rng('default') % For reproducibility

X = B.permeability;
Y = B.beta_c_model;

Dist_Corr =  distanceCorrelation(X, Y);
% p_value = permutation_test(X,Y,1000);

% estimates = [];
% observed = Dist_Corr;
% reps = 1000; % repetitions of resampling the vector
% sampled = datasample(Y,length(Y));
% 
% % trial = distanceCorrelation(X,sampled);
% for r = 1:reps
%     sampled = datasample(Y,length(Y));
%     estimates = [estimates ; distanceCorrelation(X,sampled)];
% end 
% 
% pre_p_value = estimates(estimates <= observed); % If you want to compute an empirical P-value, remember that low values of the statistic favor the alternative hypothesis estimates <= observed.
% p_value = mean(pre_p_value); 
% 
% figure(1)
% h1 = histogram(estimates);
% % xlim([0 1.0])
% 
% hold on
% line([observed, observed], ylim, 'LineWidth', 2, 'Color', 'r');
% 
% xlabel('Distance correlation coefficient');
% ylabel('Count');
% 
% % To save the figure in wanted size (Figure properties)
% set(gcf,'Units','pixels');
% set(gcf,'Position',[240 160 850 500])
% 
% saveas(figure(1),['forch_modelxperm_forch2_a.jpg']);


function CD = doubleCenter(x)
D = pdist(x);
Z = squareform(D); % Convert a vector-form distance vector to a square-form distance matrix, and vice-versa.

n = length(Z);
mean_rows = zeros(n,1);
for i = 1:n
   mean_rows(i,1) = mean(Z(i,:));  
end

mean_columns = zeros(1,n);
for j = 1:n
    mean_columns(1,j) = mean(Z(:,j));
end

mean_matrix = mean(Z,'all'); % Mean of all elements of the matrix

CD = zeros(n,n);
for k = 1:n
    for m = 1:n
        CD(k,m) = Z(k,m) - mean_rows(k,1) - mean_columns(1,m) + mean_matrix;
    end 
end
end 

function CovD = distanceCovariance(x,y)
    n = length(x); % or b because they have the same length
    CD_x = doubleCenter(x);
    CD_y = doubleCenter(y);
    
    T = (CD_x .* CD_y);
    CovD = sqrt((sum(T,'all'))/(n^2)); % sum all elements of the T matrix
end 

function VarD = distanceVariance(x)
    VarD = distanceCovariance(x,x);
end

function Dist_Corr = distanceCorrelation(x,y)
    Dist_Corr = (distanceCovariance(x,y)) ./ sqrt((distanceVariance(x) .* distanceVariance(y))); % sqrt because we want the standard deviation and not variance
end

% Statistical significance - permutation test

% function p_value = permutation_test(x,y,reps) % reps = how many repetitions for shuffling and resampling
%     estimates = [];
%     observed = distanceCorrelation(x,y);
%     
%     for r = 1:reps
%         sampled = datasample(y,length(y));
%         estimates = [estimates ; distanceCorrelation(x,sampled)];
%     end 
%    
%     pre_p_value = estimates( estimates >= observed);
%     p_value = mean(pre_p_value);
% end 

