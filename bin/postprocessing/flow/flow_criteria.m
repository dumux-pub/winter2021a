close all;
clear all;

% constant parameters for the calculation of Reynolds and Forchheimer
% number, forchConst and forchCoeff

% porosity [ - ]
% from the csv file

% the average particle diameter or pore diamaterer - for Reynolds number [ m ]
d = 0.025;

% liquid dynamic viscosity [Pa*s] - pure water
miu_liq = 1e-3;

% gas dynamic viscosity [Pa*s] - steam
miu_gas = 1e-5;

% forchConst [ - ]
c_F =  0.55;

% forchCoeff constant [ m ]
c_beta =  1.52e-4;

% create the table from the .csv output file
% first define the filename of the .csv file
filename = ('darcy_a.csv');
% filename1 = ('darcy_a.csv');
T = readtable(filename);
% A = readtable(filename1);

% tortuosity [ - ]
T.tau = 1./T.porosity; 

% relative permeabilities [ - ]
T.kr_gas = (T.mob_gas.*miu_gas);
T.kr_liq = (T.mob_liq.*miu_liq);

% beta_c - [ m^-1 ] for Forchheimer Case 1
T.beta_c = (c_F./sqrt(T.permeability));

% beta_a [ m^-1 ] - for Forchheimer Case 2
T.beta_gas = (c_beta .* T.tau)./((T.permeability.*T.kr_gas).*(T.porosity .* (1.-T.S_liq)));
T.beta_liq = (c_beta .* T.tau)./((T.permeability.*T.kr_liq).*(T.porosity .* T.S_liq));

% Reynolds number
T.Re_liq = (T.rho_liq .* T.velocity_liq_m_s__Magnitude .* d) ./ (miu_liq);
T.Re_gas = (T.rho_gas .* T.velocity_gas_m_s__Magnitude .* d) ./ (miu_gas);

% A.Re_liq = (A.rho_liq .* A.velocity_liq_m_s__Magnitude .* d) ./ (miu_liq);

% Forchheimer number
T.Fo_c = (T.rho_liq .* T.permeability.* T.beta_c .* T.velocity_liq_m_s__Magnitude) ./ (miu_liq);

T.Fo_liq = (T.rho_liq .* T.permeability .* T.mob_liq .* T.beta_liq .* T.velocity_liq_m_s__Magnitude); 
T.Fo_gas = (T.rho_gas .* T.permeability .* T.mob_gas .* T.beta_gas .* T.velocity_gas_m_s__Magnitude);

% Total velocity of the system in m/s
T.total_velocity = (T.velocity_liq_m_s__Magnitude + T.velocity_gas_m_s__Magnitude);

% descriptive statistics and info with the summary function
S = summary(T);

% filter the macropore cells with their porosity (0.90 or 0.95 depending the scenario)
rows1 = (T.porosity == 0.94); % | T.porosity == 0.90
F = T(rows1,:);

rows2 = (T.porosity == 0.90);
G = T(rows2,:);

% rows1 = (T.permeability == 1.0e-06);
% C = T(rows1,:);

% new table with different direction in rows, so the values are from top to
% bottom of the domain, to represent the flow field

B = sortrows(T,{'CellID'},{'descend'});

% histograms
figure(1)
h1 = histogram(B.Fo_liq, 94,'FaceColor','m');
xlim([0 0.35])
%title('Forchheimer number distribution')

hold on
line([0.11, 0.11], ylim, 'LineWidth', 2, 'Color', 'r');
txt = 'Fo > Fo_{c}';
text(0.12,100,txt,'FontSize',14)

set(gca,'FontSize',18)

xlabel('Forchheimer number [ - ]');
ylabel('Number of cells');

% To save the figure in wanted size (Figure properties)
set(gcf,'Units','pixels');
set(gcf,'Position',[240 160 850 500])

figure(2)
h2 = histogram(B.Re_liq, 106,'FaceColor','g');
xlim([0 2.5])
%title('Reynolds number distribution ')

hold on
line([1.0, 1.0], ylim, 'LineWidth', 2, 'Color', 'r');
txt = 'Re > Re_{c}';
text(1.1,150,txt,'FontSize',14)

xlabel('Reynolds number [ - ]');
ylabel('Number of cells');

set(gca,'FontSize',18)

% To save the figure in wanted size (Figure properties)
set(gcf,'Units','pixels');
set(gcf,'Position',[240 160 850 500])

% saveas(figure(1),['forch_num_c.jpg']);
% saveas(figure(2),['rey_num_c.jpg']);

figure(3)
% [N1,edges1] = histcounts(log10(B.Fo_c));
% h3 = histogram(B.Fo_c,10.^edges1);
% set(gca,'xscale','log')

h3 = histogram(B.Fo_c);

figure(4)
sz = 6;
scatter(T.Fo_liq,T.velocity_liq_m_s__Magnitude,sz,'filled')

% hold on 
% scatter(F.Fo_liq,F.velocity_liq_m_s__Magnitude,sz,'filled')
% 
% hold on
% scatter(G.Fo_liq,G.velocity_liq_m_s__Magnitude,sz,'filled')
set(gca,'xscale','log')
set(gca,'yscale','log')
xlabel('Fo [-]')
ylabel('v_w [m/s]')

% To save the figure in wanted size (Figure properties)
set(gcf,'Units','pixels');
set(gcf,'Position',[240 160 850 500])
% saveas(figure(3),['forch_macro_c.jpg']);

figure(5)
sz = 6;
scatter(T.Fo_c,T.velocity_liq_m_s__Magnitude,sz,'filled')

% set(gca,'xscale','log')
set(gca,'yscale','log')