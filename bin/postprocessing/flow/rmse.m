close all;
clear all;
% load the csv files of all the points in the domain

filename1 = ('darcy_a.csv'); % reference values - Darcy
A = readtable(filename1);

filename2 = ('forch1_a.csv'); % new values - Forchheimer Case 1
B = readtable(filename2);

filename3 = ('forch2_a.csv'); % new values - Forchheimer Case 2
C = readtable(filename3);

% number of observations 
n = 2500;

% values of the parameters of the models 
vel = A{:, 21};
sat = A{:, 3};
mob = A{:, 5};

vel_cf = B{:, 21};
sat_cf = B{:, 3};
mob_cf = B{:, 5};

vel_bf = C{:, 21};
sat_bf = C{:, 3};
mob_bf = C{:, 5};

% rmse calculation
rmse_satB = sqrt(sum(((sat_cf - sat).^2) ./ n));
rmse_velB = sqrt(sum(((vel_cf - vel).^2) ./ n));
rmse_mobB = sqrt(sum(((mob_cf - mob).^2) ./ n));

rmse_satC = sqrt(sum(((sat_bf - sat).^2) ./ n));
rmse_velC = sqrt(sum(((vel_bf - vel).^2) ./ n));
rmse_mobC = sqrt(sum(((mob_bf - mob).^2) ./ n));

% normalized root mean square error with the mean value of the reference
% dataset - A
mean_sat = mean(sat);
mean_vel = mean(vel);
mean_mob = mean(mob);

nrmse1_satB = rmse_satB / mean_sat;
nrmse1_velB = rmse_velB / mean_vel;
nrmse1_mobB = rmse_mobB / mean_mob;

nrmse1_satC = rmse_satC / mean_sat;
nrmse1_velC = rmse_velC / mean_vel;
nrmse1_mobC = rmse_mobC / mean_mob;

% normalized root mean square error - by dividing the difference with the
% reference dataset A 
nrmse2_satB = sqrt(sum((((sat_cf - sat) ./ sat).^2) ./ n));
nrmse2_velB = sqrt(sum((((vel_cf - vel) ./ vel).^2) ./ n));
nrmse2_mobB = sqrt(sum((((mob_cf - mob) ./ mob).^2) ./ n));

nrmse2_satC = sqrt(sum((((sat_bf - sat) ./ sat).^2) ./ n));
nrmse2_velC = sqrt(sum((((vel_bf - vel) ./ vel).^2) ./ n));
nrmse2_mobC = sqrt(sum((((mob_bf - mob) ./ mob).^2) ./ n));