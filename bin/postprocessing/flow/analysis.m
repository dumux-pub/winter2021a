close all;
clear all;

% histograms of porosities and permeabilities
 A = readtable('darcy_a.csv'); % scenario(a)
 B = readtable('darcy_b.csv'); % scenario(b)
 C = readtable('darcy_c.csv'); % scenario(c)
 
% calculate Sarle's bimodality coefficient. Equal to 5/9 uniform
% distribution, larger than 5/9 indicates bimodal distribution
g = skewness(C.porosity);
k = kurtosis(C.porosity);
ek = k - 3; % excess kurtosis
n = length(C.porosity);

b = ((g^2) + 1)/(ek + (3*((n-1)^2)/((n-2)*(n-3))));

% display message if there is indication for bimodal distribution
if b <= 5/9
    fprintf('b = %d, indicates uniform distribution \n',b);
else
    fprintf('b = %d > 5/9, indicates bimodal distribution \n',b);
end

% constant values of needed parameters
% liquid dynamic viscosity [Pa*s] - pure water
miu_liq = 1e-3;

% gravity acceleration [m/s^2]
g = 9.81;

% calculate the hydraulic conductivity [m/s] - Kozeny equation
A.hydconductivity = (A.permeability .* A.rho_liq .* g) ./ (miu_liq) ;

% descriptive statistics and info with the summary function
S = summary(A);


nbins = 30;
figure(1) % histogram of porosity for no macropore field
h1 = histogram(A.porosity,nbins);
% title('Porosity distribution')

xlabel('Porosity [-]');
ylabel('Number of cells [-]');

set(gca,'FontSize',18)

% To save the figure in wanted size (Figure properties)
set(gcf,'Units','pixels');
set(gcf,'Position',[240 160 850 500])

figure(2) % histogram of porosity for two macropores field
h2 = histogram(B.porosity,nbins);
% title('Porosity distribution')

xlabel('Porosity [-]');
ylabel('Number of cells [-]');

set(gca,'FontSize',18)

% To save the figure in wanted size (Figure properties)
set(gcf,'Units','pixels');
set(gcf,'Position',[240 160 850 500])

figure(3) % histogram of porosity for two macropores field
h3 = histogram(C.porosity,nbins);
% title('Porosity distribution')

xlabel('Porosity [-]');
ylabel('Number of cells [-]');

set(gca,'FontSize',18);

% To save the figure in wanted size (Figure properties)
set(gcf,'Units','pixels');
set(gcf,'Position',[240 160 850 500]);

saveas(figure(1),['porosity_a.jpg']);
saveas(figure(2),['porosity_b.jpg']);
saveas(figure(3),['porosity_c.jpg']);

% Work on the permeability histograms for the three samples (a), (b), (c)
permeabilities = A.permeability; % choose for which permeability field we are working
n1 = length(permeabilities);
mean_perm = mean(permeabilities);
s = std(permeabilities);
range = max(permeabilities) - min(permeabilities);

% Get the 25th and 75th percentiles of the permeabilities array
q25 = prctile(permeabilities,25);
q75 = prctile(permeabilities,75);
IQR = q75 - q25;

% lower limit
l_l = q25 - (1.5 * IQR);
% upper limit
u_l = q75 + (1.5 * IQR);

% keep the non - outlier values
non_outliers = (permeabilities >= l_l & permeabilities <= u_l);
perm_without_outliers = permeabilities(non_outliers);

% Freedman - Diaconis rule
% bin_width = 2*(q75 - q25)*(n1^(-1/3));

% Scott's rule
% bin_width = (3.5 * s) / (n1^(1/3));

% bins = round(range/bin_width);

figure(4)
bins = 30000; % really large number of bins for unfiltered dataset due to the large range of values and the dataset size
%edges = [1*10^(-12):1*10^(-5)];
h4 = histogram(permeabilities, bins);
title('Unfiltered permeability distribution')

set(gca,'xscale','log')
% xlim([1*10^(-12) 1*10^(-5)])

xlabel('Permeability [m^2]');
ylabel('Number of cells');

% ytix = get(gca, 'YTick');
% set(gca, 'YTick',ytix, 'YTickLabel',(ytix*100)/2500);

figure(5)
bins2 = 30;
h5 = histogram(perm_without_outliers,bins2);
title('Filtered permeability distribution')

% set(gca,'xscale','log')
xlabel('Permeability [m^2]');
ylabel('Number of cells');

% Group data into bins or categories - for more uniform histograms
X = C.permeability;
max_perm = max(X);
min_perm = min(X);

% divide data into N bins of uniform width and return the bin edges E
N = 90000;
[Y,edges] = discretize(X,N);

% histogram of permeability with fixed edges
figure(6)
h6 = histogram(X, edges);
title('Unfiltered permeability distribution')

set(gca,'xscale','log')
xlabel('Permeability [m^2]');
ylabel('Number of cells [-]');

% create a histogram where values are spaced evenly in bins even in log
% scale
[N1,edges1] = histcounts(log10(X));

figure(7)
h7 = histogram(X,10.^edges1);

% title('Permeability distribution')

set(gca,'xscale','log')
xlabel('Permeability [m^2]');
ylabel('Number of cells [-]');
