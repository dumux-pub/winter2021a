close all;
clear all;

% Load the csv files with the changed sat of the last row
filename = ('changedS_liq_test2p_darcy_a.csv');
A = readtable(filename);

filename = ('changedS_liq_test2p_forch1_a.csv');
B = readtable(filename);

filename = ('changedS_liq_test2p_forch2_a.csv');
C = readtable(filename);

% Create the x-axis positions at the center of each cell
x = linspace(0.1,9.9,50);

A.axis = x.';
B.axis = x.';
C.axis = x.';

T1 = movevars(A,'axis','After',1);
T2 = movevars(B,'axis','After',1);
T3 = movevars(C,'axis','After',1);
% Plot for space and time of the change in saturation
figure(1)

plot(T1.axis, T1.timestep,':','LineWidth',3)
set(gca,'yscale','log')

hold on
plot(T2.axis, T2.timestep,':','LineWidth',3)
set(gca,'yscale','log')

hold on
plot(T3.axis, T3.timestep,':','LineWidth',3)
set(gca,'yscale','log')
%set ( gca, 'ydir', 'reverse' )
% ylim([65000 105000]) % y-axis limits for scenario a
% ylim([0.04 150000]) % y-axis limits for scenarios b and c
% ylim([10^-2 10^5]) % y-axis limits for scenarios b and c of dinj
xlim([0 10]) % x-axis limits for wide macropore scenario c
ylim([6.5*10^(4) 10.5*10^(4)]) % y-axis limits for wide macropore scenario c
% 
% % Specify yticks and ylables for scenarios b and c
% yticklabels({'10^{-2}', '10^{-1}','10^{0}','10^{1}','10^{2}','10^{3}','10^{4}','10^{5}'})
% yticklabels({'0.05', '0.1', '0.15', '0.2', '0.25', '0.3'})
% yticks([0.05 0.1 0.15 0.2 0.25 0.3])

xlabel('Location [m]')
ylabel('Time [s]')

legend('Darcy','FH - Case 1','FH - Case 2','Location','northeast') 

set(gca,'FontSize',25)

% To save the figure in wanted size (Figure properties)
set(gcf,'Units','pixels');
set(gcf,'Position',[240 160 850 500])

saveas(figure(1),['darcy_forch_comp.jpg']);

% Create a figure with all three images of a scenario (a-c) together
% out = imtile({'breakthough_darcy_a.jpg', 'breakthough_forch1_a.jpg', 'breakthough_forch2_a.jpg'});
% imshow(out);
