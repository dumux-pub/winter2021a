import pyvista as pv
import numpy as np
import csv
import sys
import re
from pathlib import Path

#############################################################################################################   
def writeSatChanges(pvdFilePath, cellRange): 
### Parameters
    f = open(pvdFilePath , "rt")
    basePath = str(Path(pvdFilePath).parent.absolute()) + "/"
    vtuFiles = []
    vtuTimesteps = []
    timestep = np.zeros(cellRange)
    changedSatFile = ["" for x in range(cellRange)]
    changedSat = np.zeros(cellRange)
    s_liq = np.zeros(cellRange)

    
### Files Loop 
    for line in f:
        if "timestep" in line:
            vtuFiles.append(str(line.split('"')[-2]))
            vtuTimesteps.append(float(line.split('"')[1]))
            
### Saturation Changes Loop
    for idx,vtuFile in enumerate(vtuFiles):
        mesh = pv.read(basePath+vtuFile)
        subset = mesh.extract_cells(range(cellRange))
        s_liq = subset.get_array('S_liq')
        for cell in range(len(s_liq)):
            if abs(s_liq[cell] - 0.1) > 0.00001 and changedSat[cell] == 0:
                changedSatFile[cell] = vtuFile
                changedSat[cell] = "{:.6f}".format(s_liq[cell])
                timestep[cell] = vtuTimesteps[idx]
                print("Cell: "+ str(cell) + " changed at vtu file: " + changedSatFile[cell] + " in timestep: " + str(timestep[cell]))          

### Print Parameters          
    print(changedSat)
    print(changedSatFile)
    print(timestep)

### Write CSV
    outName = "changedS_liq_" + str((pvdFilePath.split('/')[-1].split(')')[0])) + ".csv" 
    with open(outName, 'w', newline='') as csvfile:
        fieldnames = ['cellID', 'timestep' , 'changedS_liq' ,'finalS_liq' , 'vtuFile']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for cell in range(50): 
            writer.writerow({'cellID': cell, 'timestep': timestep[cell], 'changedS_liq': changedSat[cell], 'finalS_liq': s_liq[cell], 'vtuFile': changedSatFile[cell]})
            
            
#############################################################################################################       
cellRange = 50

writeSatChanges("../../../build-cmake/appl/2p/test2p_darcy_a.pvd",cellRange)
writeSatChanges("../../../build-cmake/appl/2p/test2p_forch1_a.pvd",cellRange)
writeSatChanges("../../../build-cmake/appl/2p/test2p_forch2_a.pvd",cellRange)
