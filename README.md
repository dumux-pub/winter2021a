# Winter2021a

Welcome to the dumux pub module for Winter2021a
======================================================

This module contains the source code for the examples in the submitted
paper

__R. Winter et al. (2021)__, A study on Darcy versus Forchheimer models for flow throughheterogeneous landfills including macropores

Installation
============

For building from source create a folder and download [installWinter2021a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/winter2021a/-/blob/master/installwinter2021a.sh) from this repository

to that folder and run the script with

```
chmod u+x installwinter2021a.sh
./installwinter2021a.sh
```

Installation with Docker 
========================

Create a new folder in your favourite location and download the container startup script to that folder.
```bash
mkdir Winter2021a
cd Winter2021a
wget https://git.iws.uni-stuttgart.de/dumux-pub/winter2021a/-/raw/master/docker_winter2021a.sh
```

Open the Docker Container by running
```bash
bash docker_winter2021a.sh open
```

Reproducing the results
=======================
After the script has run successfully, you may build the program executables

```
cd winter2021a/build-cmake/appl/2p
make test_2p_tpfa
make test_2p_forchcf_tpfa
make test_2p_forchbeta_tpfa
```

and run them, e.g., with

```
./test_2p_tpfa
./test_2p_forchcf_tpfa
./test_2p_forchbeta_tpfa
```

